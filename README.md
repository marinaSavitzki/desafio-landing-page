# Landing page - desafio

# Descrição
Este projeto é uma Landing Page para captura de leads, um formulário simples e intuitivo para o usuário se inscrever e receber newsletters do site.

# Informações relevantes
No footer das pages, contém um botão, mas porquê se uma landing page não tem isso?	**R: Só para facilitar a navegação entre as páginas dos requisitos do projeto**

# O que precisa para rodar?
Instalar o [xampp](https://www.apachefriends.org/pt_br/index.html) com servidor apache e mysql 

# Como rodar?
                      
-Inicie o xampp                               
-Inicie o apache e o mysql                            
	
**IMPORTANTE !!!**  
Na PASTA DO PROJETO contém um arquivo com os comandos **SQL** necessários para a criação da base de dados no **phpmyadmin**, já que está rodando localhost.   

-Acesse em um navegador 
[phpmyadmin](http://localhost/phpmyadmin/)

-Em seguida copie os comandos SQL e na aba 'SQL' cole e execute.

-Faça o download do projeto em [link do projeto](https://bitbucket.org/marinaSavitzki/desafio-landing-page/downloads/)

-E salve na pasta **'C:\xampp\htdocs'**

-Extraia o arquivo

-Renomeie a pasta para **'desafioLandingPage'** (isto é para o link abaixo funcionar, senão é só digitar localhost/'nome da pasta'/src)

-Depois acesse em um navegador [http://localhost/desafioLandingPage/src/](http://localhost/desafioLandingPage/src/)

# Ferramentas
	IDE de Codificação                          
	 -VS Code                                   
	Banco de dados                                
	 -mySQL                                            
	Linguagem                                         
	-Backend                                             
	---PHP 7.3                                              
	-Frontend                                                
	---HTML5, CSS3 com BOOTSTRAP4, Javascript, jquery.
# Referencias
**Bootstrap** [https://getbootstrap.com/](https://getbootstrap.com/)

**Manual PHP** [https://www.php.net/manual/pt_BR/index.php](https://www.php.net/manual/pt_BR/index.php)

**Stackoverflow** [https://pt.stackoverflow.com/](https://pt.stackoverflow.com/)

# Autor(a)
	Marina Savitzki  
	Estudante de Ciencia da Computação - IFSC  
	marinasavitzki@gmail.com 